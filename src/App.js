import React from 'react';
import { Logo } from "./icon";
function App() {
  return (

    <div class="hero " >
      <div className="App" >
        <div className="section-devider"></div>
        <section className="section is-medium">
          <div className="columns">
            <div className="column is-5">

              <h1 className="home-tile has-text-white has-text-weight-bold is-size-1 is-hidden-mobile">
                You're paying way
            </h1>
              <h1 className="home-tile has-text-white has-text-weight-bold is-size-4 is-hidden-desktop is-hidden-tablet">
                You're paying way
            </h1>

              <h1 className="home-tile has-text-white has-text-weight-bold is-size-1 is-hidden-mobile">
                too much
            </h1>
              <h1 className="home-tile has-text-white has-text-weight-bold is-size-4 is-hidden-desktop is-hidden-tablet">
                too much
            </h1>
              <h1 className="home-sub-title has-text-white has-text-weight-bold is-size-3 is-hidden-mobile">
                Lets fix that... Forever
            </h1>
              <h1 className="home-tile has-text-white has-text-weight-bold is-size-6 is-hidden-desktop is-hidden-tablet">
                Lets fix that... Forever
            </h1>
              <br />
              <button class="button is-primary is-inverted has-text-black has-text-weight-bold is-size-6 is-rounded is-hidden-mobile">
                Sign in
            </button>
              <button class="button is-primary is-inverted has-text-black has-text-weight-bold is-size-7 is-rounded is-hidden-desktop is-hidden-tablet">
                Sign in
            </button>
              <button class="has-margin-left-20 button is-primary is-inverted has-text-black has-text-weight-bold is-size-6 is-rounded is-hidden-mobile">
                Learn more
            </button>
              <button class="has-margin-left-20 button is-primary is-inverted has-text-black has-text-weight-bold is-size-7 is-rounded is-hidden-desktop is-hidden-tablet">
                learn more
            </button>
            </div>
            <div className="column is-desktop is-tablet is-hidden-mobile">
              <Logo height={200} width={700} />

            </div>
          </div>
        </section>
      </div>

      <div className="hero has-background-white is-large has-bg-img">
        <div className="column">
          <div className="column">
            <div className="column is-5 is-offset-8">
              <h1 className="title has-text-success has-text-bold is-size-1">What is friftr?</h1>
              <p clasName="has-text-left">
                Friftr is a pro consumer platform to help people <br />
                find and share better and cheaper alternatives to <br />
                overpriced product          </p>
            </div>


            <div className="column is-full" >
              <h1 className="title has-text-success has-text-bold is-size-2">Find Better AND Cheaper</h1>
              <p clasName="has-text-left">
                Sometimes &nbsp; the &nbsp; best &nbsp; thing &nbsp; isn't &nbsp; always &nbsp; the &nbsp; most<br />
                Expensive thing. we don't believe in brand loyalty. &nbsp;we <br />
                believe in saving money without comprommising quality <br />
                in the precess!          </p>
            </div>

            <div className="column is-5 is-offset-8">
              <h1 className="title has-text-success has-text-bold is-size-2">Find a good deal</h1>
              <p clasName="has-text-left">
                Everyone Knows a good deal. Why not work <br />
                together?        </p>
            </div>

            <div className="column is-full">
              <h1 className="title has-text-success has-text-bold is-size-2">Find Better AND Cheaper</h1>
              <p clasName="has-text-left">
                Sometimes &nbsp; the &nbsp; best &nbsp; thing &nbsp; isn't &nbsp; always &nbsp; the &nbsp; most<br />
                Expensive thing. we don't believe in brand loyalty. &nbsp;we <br />
                believe in saving money without comprommising quality <br />
                in the precess!          </p>
            </div>

          </div>
        </div>
      </div>

      <div className="section-3 hero is-success">
        <div className="column is-5 is-offset-5">
          <div className="has-text-centered has-margin-top-140">
            <h1 className="title has-text-white has-text-bold is-size-1" >Vote for the best</h1>
            <h3 className="subtitle has-text-white has-text-bold is-size-6" >Let everyone else else know the best deals are.</h3>
          </div>
        </div>
      </div>

      <div className="section-3 hero is-primary">
        <div className="column is-5 is-offset-5">
          <div className="has-margin-top-140">
            <h1 className="title has-text-black has-text-centered  is-size-1">Quality Control</h1>
            <p className="has-text-black has-text-centered margin-left-100 is-size-6">We have built tools to make sure only <br />
              the best comes through. All slam,no <br />
              spam </p>
          </div>
        </div>

      </div>

      <div className="footer">
        <div className="footer-devider"></div>
        <section className="section is-medium">
          <div className="columns">
            <div className="column is-5">
              <h1 className="home-tile has-text-white has-text-weight-bold is-size-1 is-hidden-mobile">
                You're paying way
            </h1>
              <h1 className="home-tile has-text-white has-text-weight-bold is-size-4 is-hidden-desktop is-hidden-tablet">
                You're paying way
            </h1>

              <h1 className="home-tile has-text-white has-text-weight-bold is-size-1 is-hidden-mobile">
                too much
            </h1>
              <h1 className="home-tile has-text-white has-text-weight-bold is-size-4 is-hidden-desktop is-hidden-tablet">
                too much
            </h1>
              <h1 className="home-sub-title has-text-white has-text-weight-bold is-size-3 is-hidden-mobile">
                Lets fix that... Forever
            </h1>
              <h1 className="home-tile has-text-white has-text-weight-bold is-size-6 is-hidden-desktop is-hidden-tablet">
                Lets fix that... Forever
            </h1>
              <br />
              <button class="button is-primary is-inverted has-text-black has-text-weight-bold is-size-6 is-rounded is-hidden-mobile">
                Sign in
            </button>
              <button class="button is-primary is-inverted has-text-black has-text-weight-bold is-size-7 is-rounded is-hidden-desktop is-hidden-tablet">
                Sign in
            </button>
              <button class="has-margin-left-20 button is-primary is-inverted has-text-black has-text-weight-bold is-size-6 is-rounded is-hidden-mobile">
                Learn more
            </button>
              <button class="has-margin-left-20 button is-primary is-inverted has-text-black has-text-weight-bold is-size-7 is-rounded is-hidden-desktop is-hidden-tablet">
                learn more
            </button>
            </div>
            <div className="column is-desktop is-tablet is-hidden-mobile">
              <Logo height={200} width={700} />
            </div>
          </div>
        </section>
      </div>
    </div>
  );
}

export default App;
